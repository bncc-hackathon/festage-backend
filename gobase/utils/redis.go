package utils

import (
	"fmt"
	"github.com/go-redis/redis"
	"os"
	"time"
)

func GetClient() (*redis.Client, error) {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	address := host + ":" + port

	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "",
		DB:       0,
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}

	return client, nil
}

func Validate(key string, value string) (bool, error) {
	client, err := GetClient()
	if err != nil {
		return false, err
	}

	result, err := client.Get(key).Result()
	fmt.Println(err)
	if err != nil {
		fmt.Println(err)
		return false, err
	}

	isValid := value == result
	return isValid, nil
}

func Set(key string, value string, duration time.Duration) (bool, error) {
	client, err := GetClient()
	if err != nil {
		return false, err
	}

	err = client.Set(key, value, duration).Err()
	if err != nil {
		return false, err
	}

	return true, nil
}

func Del(key string) (bool, error) {
	client, err := GetClient()
	if err != nil {
		return false, err
	}

	err = client.Del(key).Err()
	if err != nil {
		return false, err
	}

	return true, nil
}
