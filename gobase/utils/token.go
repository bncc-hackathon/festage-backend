package utils

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"net/http"
	"strconv"
	"time"
)

func TokenValidation(w http.ResponseWriter, r *http.Request) (uint, error) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	token := r.Header.Get("Bearer-Token")
	if token == "" {
		response := responses.BaseResponse{
			Message: http.StatusText(http.StatusUnauthorized),
			Code:    http.StatusUnauthorized,
			Error:   "no token provided",
			Data:    "no token provided",
		}
		WriteResponse(w, response, http.StatusUnauthorized)

		return 0, errors.New("no token provided")
	}

	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte("kale"), nil
	})
	if err != nil {
		return 0, err
	}

	claims := parsedToken.Claims.(jwt.MapClaims)
	userId := uint(claims["id"].(float64))

	timeStamp := int64(claims["created_at"].(float64))
	now := time.Now().Unix()

	differenceInSecond := now - timeStamp
	if differenceInSecond > 2*60*60 {
		response := responses.BaseResponse{
			Message: http.StatusText(http.StatusUnauthorized),
			Code:    http.StatusUnauthorized,
			Error:   "session expired",
			Data:    "session expired",
		}
		WriteResponse(w, response, http.StatusUnauthorized)

		return 0, errors.New("session expired")
	}

	isValid, err := Validate(strconv.Itoa(int(userId)), token)
	if err != nil {
		response := responses.BaseResponse{
			Message: http.StatusText(http.StatusUnauthorized),
			Code:    http.StatusUnauthorized,
			Error:   "invalid token",
			Data:    "invalid token",
		}
		WriteResponse(w, response, http.StatusUnauthorized)

		return 0, errors.New("invalid token")
	}

	if !isValid {
		response := responses.BaseResponse{
			Message: http.StatusText(http.StatusUnauthorized),
			Code:    http.StatusUnauthorized,
			Error:   "token is not valid",
			Data:    "token is not valid",
		}
		WriteResponse(w, response, http.StatusUnauthorized)

		return 0, errors.New("token is not valid")
	} else {
		return userId, nil
	}
}
