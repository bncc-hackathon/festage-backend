package utils

import (
	"encoding/json"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"net/http"
)

func WriteResponse(w http.ResponseWriter, value interface{}, code int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(value)
}

func WriteError(w http.ResponseWriter, err error) {
	er := responses.BaseResponse{
		Message: http.StatusText(http.StatusBadRequest),
		Code:    http.StatusBadRequest,
		Error:   err.Error(),
		Data:    err.Error(),
	}
	WriteResponse(w, er, http.StatusBadRequest)
}
