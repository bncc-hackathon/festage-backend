package responses

type BaseResponse struct {
	Message string      `json:"message"`
	Code    int         `json:"code"`
	Error   string      `json:"error"`
	Data    interface{} `json:"data"`
}
