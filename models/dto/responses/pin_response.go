package responses

import "time"

type PinResponse struct {
	ID        uint      `json:"id"`
	Pin       string    `json:"-"`
	IsValid   bool      `json:"isValid"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
