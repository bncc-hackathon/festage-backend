package responses

import (
	"github.com/gofrs/uuid"
	"time"
)

type ConnectionResponse struct {
	ID        uint      `json:"id"`
	UserId    uint      `json:"user_id"`
	Name      string    `json:"name"`
	Token     uuid.UUID `json:"token"`
	IsValid   bool      `json:"is_valid"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
