package entity

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name      string
	Email     string `gorm:"unique"`
	Password  string
	Role      uint
	ImagePath string `json:"image_path"`
}
