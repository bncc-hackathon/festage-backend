package entity

import (
	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type Connection struct {
	gorm.Model
	UserId  uint
	Name    string
	Token   uuid.UUID
	IsValid bool
}
