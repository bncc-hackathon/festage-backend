package entity

import "gorm.io/gorm"

type Pin struct {
	gorm.Model
	Pin     string
	IsValid bool
}
