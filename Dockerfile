FROM alpine:3.12
RUN apk add --no-cache curl tzdata
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
COPY ./bin/main /bin/main
COPY .env /.env
ENTRYPOINT /bin/main serve