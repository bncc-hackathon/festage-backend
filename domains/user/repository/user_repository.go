package repository

import (
	"github.com/wincentrtz/gobase/domains/user"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
	"gorm.io/gorm"
)

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) user.Repository {
	return &userRepository{
		db: db,
	}
}

func (ur *userRepository) FetchAll() (*[]responses.UserResponse, error) {
	var results []entity.User
	users := make([]responses.UserResponse, 0)

	err := ur.db.Find(&results)
	if err.Error != nil {
		return nil, err.Error
	}

	for _, result := range results {
		users = append(users, responses.UserResponse{
			ID:        result.ID,
			Name:      result.Name,
			Email:     result.Email,
			Role:      result.Role,
			ImagePath: result.ImagePath,
			CreatedAt: result.CreatedAt,
			UpdatedAt: result.UpdatedAt,
		})
	}

	return &users, nil
}

func (ur *userRepository) FetchUserById(userId int) (*responses.UserResponse, error) {
	result := entity.User{}
	err := ur.db.First(&result, userId)
	if err.Error != nil {
		return nil, err.Error
	}

	userResponse := &responses.UserResponse{
		ID:        result.ID,
		Name:      result.Name,
		Email:     result.Email,
		Role:      result.Role,
		ImagePath: result.ImagePath,
		CreatedAt: result.CreatedAt,
		UpdatedAt: result.UpdatedAt,
	}

	return userResponse, nil
}

func (ur *userRepository) FetchConnectionByToken(token string) (*entity.Connection, error) {
	connection := entity.Connection{}
	err := ur.db.Where("token = ? AND is_valid = ?", token, true).First(&connection)
	if err.Error != nil {
		return nil, err.Error
	}

	return &connection, nil
}

func (ur *userRepository) FetchUserByEmail(email string) (*responses.UserResponse, error) {
	result := entity.User{}
	ur.db.Where("email = ?", email).First(&result)

	userResponse := &responses.UserResponse{
		ID:        result.ID,
		Name:      result.Name,
		Email:     result.Email,
		Role:      result.Role,
		ImagePath: result.ImagePath,
		Password:  result.Password,
		CreatedAt: result.CreatedAt,
		UpdatedAt: result.UpdatedAt,
	}

	return userResponse, nil
}

func (ur *userRepository) CreateUser(user *entity.User) (*responses.UserResponse, error) {
	result := ur.db.Create(user)
	if result.Error != nil {
		return nil, result.Error
	}

	createdUser, err := ur.FetchUserById(int(user.ID))
	return createdUser, err
}
