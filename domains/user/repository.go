package user

import (
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
)

type Repository interface {
	FetchAll() (*[]responses.UserResponse, error)
	FetchUserById(userId int) (*responses.UserResponse, error)
	FetchUserByEmail(email string) (*responses.UserResponse, error)
	FetchConnectionByToken(token string) (*entity.Connection, error)
	CreateUser(user *entity.User) (*responses.UserResponse, error)
}
