package handler

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gofrs/uuid"
	"github.com/wincentrtz/gobase/models/entity"
	"golang.org/x/crypto/bcrypt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/wincentrtz/gobase/domains/user/repository"
	"github.com/wincentrtz/gobase/domains/user/usecase"
	"github.com/wincentrtz/gobase/models/dto/responses"

	"github.com/gorilla/mux"
	"github.com/wincentrtz/gobase/domains/user"
	"github.com/wincentrtz/gobase/gobase/infrastructures/db"
	"github.com/wincentrtz/gobase/gobase/utils"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type UserHandler struct {
	UserUsecase user.Usecase
}

func NewUserHandler(r *mux.Router) {
	database := db.Postgres()
	ur := repository.NewUserRepository(database)
	us := usecase.NewUserUsecase(ur)
	handler := &UserHandler{
		UserUsecase: us,
	}

	r.HandleFunc("/api/user/all", handler.FetchAll).Methods(http.MethodGet)
	r.HandleFunc("/api/user/{id}", handler.FindById).Methods(http.MethodGet)
	r.HandleFunc("/api/user/image", handler.UploadUserImage).Methods(http.MethodPost)
	r.HandleFunc("/api/user/login", handler.Login).Methods(http.MethodPost)
	r.HandleFunc("/api/user/register", handler.Register).Methods(http.MethodPost)
}

func (uh *UserHandler) FetchAll(w http.ResponseWriter, r *http.Request) {
	_, err := utils.TokenValidation(w, r)
	if err != nil {
		return
	}

	result, err := uh.UserUsecase.FetchAll()
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	resp := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    result,
	}
	utils.WriteResponse(w, resp, http.StatusOK)
}

func (uh *UserHandler) FindById(w http.ResponseWriter, r *http.Request) {
	_, err := utils.TokenValidation(w, r)
	if err != nil {
		return
	}

	err = uh.ValidateConnection(r)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	vars := mux.Vars(r)
	i, _ := strconv.Atoi(vars["id"])

	result, err := uh.UserUsecase.FetchUserById(i)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	resp := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    result,
	}
	utils.WriteResponse(w, resp, http.StatusOK)
}

func (uh *UserHandler) Register(w http.ResponseWriter, r *http.Request) {
	result := entity.User{}
	err := json.NewDecoder(r.Body).Decode(&result)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	err = uh.ValidateConnection(r)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(result.Password), bcrypt.DefaultCost)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	result.Password = string(hash)

	createdUser, err := uh.UserUsecase.CreateUser(&result)
	if err != nil {
		utils.WriteError(w, err)
		return
	} else {
		resp := &responses.BaseResponse{
			Message: http.StatusText(http.StatusOK),
			Code:    http.StatusOK,
			Data:    createdUser,
		}
		utils.WriteResponse(w, resp, http.StatusOK)
	}
}

func (uh *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	result := entity.User{}
	err := json.NewDecoder(r.Body).Decode(&result)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	err = uh.ValidateConnection(r)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	matchedUser, err := uh.UserUsecase.FetchUserByEmail(result.Email)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(matchedUser.Password), []byte(result.Password))
	if err != nil {
		utils.WriteError(w, errors.New("no user matched"))
		return
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":         matchedUser.ID,
		"created_at": time.Now().Unix(),
	}).SignedString([]byte("kale"))
	if err != nil {
		utils.WriteError(w, errors.New("no user matched"))
		return
	}

	matchedUser.Token = token
	isSuccess, err := utils.Set(strconv.Itoa(int(matchedUser.ID)), token, 2*60*60*time.Second)
	if err != nil || !isSuccess {
		utils.WriteError(w, err)
		return
	}

	resp := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    matchedUser,
	}
	utils.WriteResponse(w, resp, http.StatusOK)
}

func (uh *UserHandler) UploadUserImage(w http.ResponseWriter, r *http.Request) {
	err := uh.ValidateConnection(r)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	token, err := uuid.DefaultGenerator.NewV1()
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	err = SaveTemporaryFile(r, token.String()+".jpg")
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	imageUrl, err := S3Uploader(token.String() + ".jpg")
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	resp := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    imageUrl,
	}
	utils.WriteResponse(w, resp, http.StatusOK)
}

func SaveTemporaryFile(r *http.Request, filename string) error {
	file, _, err := r.FormFile("file")
	if err != nil {
		return errors.New("temp: " + err.Error())
	}
	defer file.Close()

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return errors.New("temp: " + err.Error())
	}
	defer f.Close()
	_, _ = io.Copy(f, file)

	return nil
}

func S3Uploader(filename string) (string, error) {
	awsId := os.Getenv("AWS_ID")
	awsSecret := os.Getenv("AWS_SECRET")
	credential := credentials.NewStaticCredentials(awsId, awsSecret, "")

	_, err := credential.Get()
	if err != nil {
		return "", err
	}

	awsSession, err := session.NewSession(&aws.Config{
		Region:      aws.String("us-east-2"),
		Credentials: credential,
	})
	if err != nil {
		return "", err
	}

	svc := s3.New(awsSession)
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}

	defer file.Close()

	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size)

	_, err = file.Read(buffer)
	if err != nil {
		return "", err
	}

	fileBytes := bytes.NewReader(buffer)
	fileType := http.DetectContentType(buffer)
	path := "/media/" + filename
	params := &s3.PutObjectInput{
		Bucket:        aws.String(os.Getenv("AWS_BUCKET")),
		Key:           aws.String(path),
		Body:          fileBytes,
		ContentLength: aws.Int64(size),
		ContentType:   aws.String(fileType),
		ACL:           aws.String("public-read"),
	}

	_, err = svc.PutObject(params)
	if err != nil {
		return "", err
	}

	err = os.Remove(filename)
	if err != nil {
		return "", err
	}

	imageUrl := "https://gobase-groupa.s3.us-east-2.amazonaws.com/media/" + filename
	return imageUrl, nil
}

func (uh *UserHandler) ValidateConnection(r *http.Request) error {
	connection := r.Header.Get("GAuth-Token")
	if connection == "" {
		return errors.New("no GAuth token")
	}

	result, err := uh.UserUsecase.FetchConnectionByToken(connection)
	fmt.Println(result, err)
	if err != nil {
		return errors.New("GAuth: " + err.Error())
	}

	return nil
}
