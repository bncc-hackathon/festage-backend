package usecase

import (
	"github.com/wincentrtz/gobase/domains/user"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
)

type userUsecase struct {
	userRepo user.Repository
}

func NewUserUsecase(ur user.Repository) user.Usecase {
	return &userUsecase{
		userRepo: ur,
	}
}

func (pu *userUsecase) FetchAll() (*[]responses.UserResponse, error) {
	result, err := pu.userRepo.FetchAll()
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (pu *userUsecase) FetchUserById(userId int) (*responses.UserResponse, error) {
	result, err := pu.userRepo.FetchUserById(userId)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (pu *userUsecase) FetchUserByEmail(email string) (*responses.UserResponse, error) {
	result, err := pu.userRepo.FetchUserByEmail(email)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (pu *userUsecase) FetchConnectionByToken(token string) (*entity.Connection, error) {
	result, err := pu.userRepo.FetchConnectionByToken(token)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (pu *userUsecase) CreateUser(user *entity.User) (*responses.UserResponse, error) {
	result, err := pu.userRepo.CreateUser(user)
	if err != nil {
		return nil, err
	}
	return result, nil
}
