package handler

import (
	"encoding/json"
	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"github.com/wincentrtz/gobase/domains/admin"
	"github.com/wincentrtz/gobase/domains/admin/repository"
	"github.com/wincentrtz/gobase/domains/admin/usecase"
	"github.com/wincentrtz/gobase/gobase/infrastructures/db"
	"github.com/wincentrtz/gobase/gobase/utils"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
	"net/http"
	"strconv"
)

type AdminHandler struct {
	adminUsecase admin.AdminUsecase
}

func NewAdminHandler(r *mux.Router) {
	database := db.Postgres()
	ur := repository.NewAdminRepository(database)
	us := usecase.NewAdminUsecase(ur)
	handler := &AdminHandler{
		adminUsecase: us,
	}

	r.HandleFunc("/api/admin/connection/all", handler.FetchAll).Methods("GET")
	r.HandleFunc("/api/admin/connection/me", handler.FindByUserId).Methods("GET")
	r.HandleFunc("/api/admin/connection/revoke", handler.RevokeConnection).Methods("POST")
	r.HandleFunc("/api/admin/connection/generate", handler.GenerateConnection).Methods("POST")

	r.HandleFunc("/api/admin/token/revoke", handler.RevokeToken).Methods("POST")
}

func (ah *AdminHandler) FetchAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	result, err := ah.adminUsecase.FetchAll()
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	response := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    result,
	}
	utils.WriteResponse(w, response, http.StatusOK)
}

func (ah *AdminHandler) FindByUserId(w http.ResponseWriter, r *http.Request) {
	userId, err := utils.TokenValidation(w, r)
	if err != nil {
		return
	}

	result, err := ah.adminUsecase.FetchByUserId(userId)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	response := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    result,
	}
	utils.WriteResponse(w, response, http.StatusOK)
}

func (ah *AdminHandler) GenerateConnection(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	connection := entity.Connection{}
	err := json.NewDecoder(r.Body).Decode(&connection)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	token, err := uuid.DefaultGenerator.NewV1()
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	connection.IsValid = true
	connection.Token = token

	result, err := ah.adminUsecase.CreateConnection(&connection)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	response := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    result,
	}
	utils.WriteResponse(w, response, http.StatusOK)
}

func (ah *AdminHandler) RevokeConnection(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	connection := entity.Connection{}
	err := json.NewDecoder(r.Body).Decode(&connection)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	result, err := ah.adminUsecase.FetchByConnectionId(connection.ID)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	result.IsValid = false
	updatedData, err := ah.adminUsecase.UpdateConnection(result)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	response := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    updatedData,
	}
	utils.WriteResponse(w, response, http.StatusOK)
}

func (ah *AdminHandler) RevokeToken(w http.ResponseWriter, r *http.Request) {
	userId, err := utils.TokenValidation(w, r)
	if err != nil {
		return
	}

	result, err := utils.Del(strconv.Itoa(int(userId)))
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	response := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    result,
	}
	utils.WriteResponse(w, response, http.StatusOK)
}
