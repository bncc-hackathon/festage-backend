package admin

import (
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
)

type AdminUsecase interface {
	FetchAll() (*[]responses.ConnectionResponse, error)
	FetchByUserId(userId uint) (*[]responses.ConnectionResponse, error)
	FetchByConnectionId(connectionId uint) (*entity.Connection, error)
	CreateConnection(connection *entity.Connection) (*responses.ConnectionResponse, error)
	UpdateConnection(connection *entity.Connection) (*responses.ConnectionResponse, error)
}
