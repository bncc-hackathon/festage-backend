package repository

import (
	"github.com/wincentrtz/gobase/domains/admin"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
	"gorm.io/gorm"
)

type adminRepository struct {
	db *gorm.DB
}

func NewAdminRepository(db *gorm.DB) admin.AdminRepository {
	return &adminRepository{
		db: db,
	}
}

func (ar *adminRepository) FetchAll() (*[]responses.ConnectionResponse, error) {
	var results []entity.Connection
	connections := make([]responses.ConnectionResponse, 0)

	err := ar.db.Find(&results)
	if err.Error != nil {
		return nil, err.Error
	}

	for _, connection := range results {
		connections = append(connections, responses.ConnectionResponse{
			ID:        connection.ID,
			Name:      connection.Name,
			Token:     connection.Token,
			IsValid:   connection.IsValid,
			CreatedAt: connection.CreatedAt,
			UpdatedAt: connection.UpdatedAt,
		})
	}

	return &connections, nil
}

func (ar *adminRepository) FetchByUserId(userId uint) (*[]responses.ConnectionResponse, error) {
	var connections []entity.Connection
	results := make([]responses.ConnectionResponse, 0)

	err := ar.db.Where("user_id = ? AND is_valid = ?", userId, true).Find(&connections)
	if err.Error != nil {
		return nil, err.Error
	}

	for _, connection := range connections {
		results = append(results, responses.ConnectionResponse{
			ID:        connection.ID,
			UserId:    connection.UserId,
			Name:      connection.Name,
			Token:     connection.Token,
			IsValid:   connection.IsValid,
			CreatedAt: connection.CreatedAt,
			UpdatedAt: connection.UpdatedAt,
		})
	}

	return &results, nil
}

func (ar *adminRepository) FetchByConnectionId(connectionId uint) (*entity.Connection, error) {
	connection := entity.Connection{}
	err := ar.db.First(&connection, connectionId)
	if err.Error != nil {
		return nil, err.Error
	}

	return &connection, nil
}

func (ar *adminRepository) CreateConnection(connection *entity.Connection) (*responses.ConnectionResponse, error) {
	result := ar.db.Create(connection)
	if result.Error != nil {
		return nil, result.Error
	}

	createdConnection, err := ar.FetchByConnectionId(connection.ID)
	if err != nil {
		return nil, err
	}

	response := &responses.ConnectionResponse{
		ID:        createdConnection.ID,
		UserId:    createdConnection.UserId,
		Name:      createdConnection.Name,
		Token:     createdConnection.Token,
		IsValid:   createdConnection.IsValid,
		CreatedAt: createdConnection.CreatedAt,
		UpdatedAt: createdConnection.UpdatedAt,
	}

	return response, err
}

func (ar *adminRepository) UpdateConnection(connection *entity.Connection) (*responses.ConnectionResponse, error) {
	result := ar.db.Save(&connection)
	if result.Error != nil {
		return nil, result.Error
	}

	updatedConnection, err := ar.FetchByConnectionId(connection.ID)
	if err != nil {
		return nil, err
	}

	response := &responses.ConnectionResponse{
		ID:        updatedConnection.ID,
		UserId:    updatedConnection.UserId,
		Name:      updatedConnection.Name,
		Token:     updatedConnection.Token,
		IsValid:   updatedConnection.IsValid,
		CreatedAt: updatedConnection.CreatedAt,
		UpdatedAt: updatedConnection.UpdatedAt,
	}

	return response, err
}
