package usecase

import (
	"github.com/wincentrtz/gobase/domains/admin"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
)

type adminUsecase struct {
	adminRepo admin.AdminRepository
}

func NewAdminUsecase(tr admin.AdminRepository) admin.AdminUsecase {
	return &adminUsecase{
		adminRepo: tr,
	}
}

func (au *adminUsecase) FetchAll() (*[]responses.ConnectionResponse, error) {
	result, err := au.adminRepo.FetchAll()
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (au *adminUsecase) FetchByUserId(userId uint) (*[]responses.ConnectionResponse, error) {
	result, err := au.adminRepo.FetchByUserId(userId)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (au *adminUsecase) FetchByConnectionId(connectionId uint) (*entity.Connection, error) {
	result, err := au.adminRepo.FetchByConnectionId(connectionId)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (au *adminUsecase) CreateConnection(connection *entity.Connection) (*responses.ConnectionResponse, error) {
	result, err := au.adminRepo.CreateConnection(connection)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (au *adminUsecase) UpdateConnection(connection *entity.Connection) (*responses.ConnectionResponse, error) {
	result, err := au.adminRepo.UpdateConnection(connection)
	if err != nil {
		return nil, err
	}
	return result, nil
}
