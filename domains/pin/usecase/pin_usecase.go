package usecase

import (
	"github.com/wincentrtz/gobase/domains/pin"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
)

type pinUsecase struct {
	pinRepo pin.PinRepository
}

func NewPinUsecase(pr pin.PinRepository) pin.PinUsecase {
	return &pinUsecase{
		pinRepo: pr,
	}
}

func (pu *pinUsecase) CreatePin(pin *entity.Pin) (*responses.PinResponse, error) {
	result, err := pu.pinRepo.CreatePin(pin)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (pu *pinUsecase) FetchPinByUserId(userId uint) (*responses.PinResponse, error) {
	result, err := pu.pinRepo.FetchPinByUserId(userId)
	if err != nil {
		return nil, err
	}
	return result, nil
}
