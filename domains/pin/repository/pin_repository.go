package repository

import (
	"github.com/wincentrtz/gobase/domains/pin"
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
	"gorm.io/gorm"
)

type pinRepository struct {
	db *gorm.DB
}

func NewPinRepository(db *gorm.DB) pin.PinRepository {
	return &pinRepository{
		db: db,
	}
}

func (pr *pinRepository) CreatePin(pin *entity.Pin) (*responses.PinResponse, error) {
	result := pr.db.Create(pin)
	if result.Error != nil {
		return nil, result.Error
	}

	createdUser, err := pr.FetchPinByUserId(pin.ID)
	return createdUser, err
}

func (pr *pinRepository) FetchPinByUserId(userId uint) (*responses.PinResponse, error) {
	result := entity.Pin{}
	err := pr.db.First(&result, userId)
	if err.Error != nil {
		return nil, err.Error
	}

	pinResponse := &responses.PinResponse{
		ID:        result.ID,
		Pin:       result.Pin,
		IsValid:   result.IsValid,
		CreatedAt: result.CreatedAt,
		UpdatedAt: result.UpdatedAt,
	}

	return pinResponse, nil
}
