package pin

import (
	"github.com/wincentrtz/gobase/models/dto/responses"
	"github.com/wincentrtz/gobase/models/entity"
)

type PinRepository interface {
	CreatePin(pin *entity.Pin) (*responses.PinResponse, error)
	FetchPinByUserId(userId uint) (*responses.PinResponse, error)
}
