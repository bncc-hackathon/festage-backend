package handler

import (
	"encoding/json"
	"errors"
	"github.com/wincentrtz/gobase/domains/pin"
	"github.com/wincentrtz/gobase/domains/pin/repository"
	"github.com/wincentrtz/gobase/domains/pin/usecase"
	"github.com/wincentrtz/gobase/models/entity"
	"golang.org/x/crypto/bcrypt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/wincentrtz/gobase/gobase/infrastructures/db"
	"github.com/wincentrtz/gobase/gobase/utils"
	"github.com/wincentrtz/gobase/models/dto/responses"
)

type PinHandler struct {
	pinUsecase pin.PinUsecase
}

func NewPinHandler(r *mux.Router) {
	database := db.Postgres()
	ur := repository.NewPinRepository(database)
	us := usecase.NewPinUsecase(ur)
	handler := &PinHandler{
		pinUsecase: us,
	}
	r.HandleFunc("/api/pin/create", handler.CreatePin).Methods("POST")
	r.HandleFunc("/api/pin/validate", handler.ValidatePin).Methods("POST")
}

func (ph *PinHandler) CreatePin(w http.ResponseWriter, r *http.Request) {
	userId, err := utils.TokenValidation(w, r)
	if err != nil {
		return
	}

	result := entity.Pin{}
	err = json.NewDecoder(r.Body).Decode(&result)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(result.Pin), bcrypt.DefaultCost)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	result.ID = userId
	result.Pin = string(hash)
	result.IsValid = true

	createdUser, err := ph.pinUsecase.CreatePin(&result)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	resp := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    createdUser,
	}
	utils.WriteResponse(w, resp, http.StatusOK)
}

func (ph *PinHandler) ValidatePin(w http.ResponseWriter, r *http.Request) {
	userId, err := utils.TokenValidation(w, r)
	if err != nil {
		return
	}

	result := entity.Pin{}
	err = json.NewDecoder(r.Body).Decode(&result)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	matchedUser, err := ph.pinUsecase.FetchPinByUserId(userId)
	if err != nil {
		utils.WriteError(w, err)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(matchedUser.Pin), []byte(result.Pin))
	if err != nil {
		utils.WriteError(w, errors.New("wrong pin"))
		return
	}

	resp := &responses.BaseResponse{
		Message: http.StatusText(http.StatusOK),
		Code:    http.StatusOK,
		Data:    matchedUser,
	}
	utils.WriteResponse(w, resp, http.StatusOK)
}
